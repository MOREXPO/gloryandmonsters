# GloryAndMonsters
Proyecto de fin de curso realizado por Iago Moreda

## Proyecto subido 1 
*24/03/2020*

Cree la interfaz de forma basica para el menu,ademas de programar un Script para que sea funcional.

## Proyecto subido 2
*25/03/2020*

Cree el terreno de la escena 1, ademas del personaje principal y realice un script  para controlar sus movimientos y las animaciones de quedarse quieto y caminar.Por ultimo desarrolle las colisiones entre el personaje y las diferentes partes del terreno.

## Proyecto subido 3
*28/03/2020*

Cree el terreno de las distintas casas del pueblo con sus colisiones.Además cree una forma para telenstransportarse entre los distintos terrenos.

## Proyecto subido 4
*29/03/2020*

Cree un contenedor a la derecha que contiene la barra de vida,la barra de mana, el nivel del personaje y los stats(Vida,Resistencia,Fuerza y magia).

## Proyecto subido 5
*31/03/2020*

Le puse a la camara los limites del mapa para que no lo sobrepase,ademas le añadi transiciones cuando pasas de una escena a otra ,y por ultimo, le meti un efecto de suavidad en el movimiento de la camara. 

## Proyecto subido 6
*01/04/2020*

Hice para que el personaje pueda atacar con la espada,tambien un script para que un objeto pueda ser destruido y por ultimo un script que mejora el efecto de las profundidades.

## Proyecto subido 7
*03/04/2020*

Desarrolle el ataque magico con espada para que pueda atacar a distancia,ademas de todo el sistema de los stats,incluyendo Vida,Fuerza,Resistencia y la Magia la cual esta controlada por el maná,tambien implemente la experiencia la cual obtienes de los enemigos,de momento desarrolle al troncogro, al cual podemos matar y cuales pueden matarnos.Ademas cada vez que subas de nivel ya podrás subir alguno de los Stats.

## Proyecto subido 8
*04/04/2020*

Cree un NPC, con el que podras tener dialogos gracias al Script que realice.

## Proyecto subido 9
*06/04/2020*

Modifique las cámaras y el Script de la cámara para que funcione mediante cinemachine, ademas metí más NPCS y les metí animaciones y arregle un poco su Script.Además metí un gato que persigue a un ratón para rellenar.

## Proyecto subido 10
*08/04/2020*

Tuve que actualizar unity a la versión de 2019 para incorporar unas funcionalidades que necesitaba,gracias a eso realize una cinematica al inicio.Pude desarrollar el comienzo del juego donde te piden el nombre y gracias a la transición pasa al juego.Por ultimo cambie algunos dialogos de los NPCS en el juego.

## Proyecto subido 11
*10/04/2020*

Cree el escenario2, y algunos NPCS en él.Además arregle un error que tenia cuando hacia el ataque mágico.

## Proyecto subido 12
*12/04/2020*

Cree el escenario3, y le incluí troncogros.

## Proyecto subido 13
*20/04/2020*

En el escenario 3 le incluí globins y modifique ciertos parámetros en los enemigos como el nivel, daño,vida... , ademas cree el escenario 4 donde añadí muchos enemigos y una cinemática cuando cojas la reliquia y cree el escenario 5 donde añadí 3 golems.

## Proyecto subido 14
*26/04/2020*

Cree el escenario 6 y 7,asi como sus enemigos,nigromantes y el jefe final y por ultimo le añadi sonidos al juego.

## Proyecto subido 15
*01/05/2020*

Le metí una pausa al juego desde donde puedes salir al menú o cambiar las opciones de sonido, ademas cambie valores en algunos enemigos y en la experiencia, tambien arregle un problema que tenía con la resistencia e hice que el ataque mágico pueda atravesar a los enemigos, y por último le metí la opción de saltarse la introducción.

## Proyecto subido 16
*05/05/2020*

Desarrolle todo el sistema de guardado y carga, y cambie la cámara para que se vea más y mejor.

## Proyecto subido 17
*06/05/2020*

Documenté todo el codigo de los Scripts.

