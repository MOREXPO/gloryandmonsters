﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja los objetos destruibles.
/// </summary>
public class Destroyable : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="string"/>
    /// </summary>
    public string destroyState;
    /// <summary>
    /// Objeto de tipo <see cref="float"/>
    /// </summary>
    public float timeForDisable;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        anim = GetComponent<Animator>();
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
    }
    /// <summary>
    /// Si <see cref="Attack"/> choca contra el collider se destruye el <see cref="GameObject"/>.
    /// </summary>
    /// <param name="collision">El objeto que choca contra el collider</param>
    /// <returns>Retardo de tiempo para desactivar el GameObject</returns>
    private IEnumerator OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Attack") {
            anim.Play(destroyState);
            audioControl.Play();
            yield return new WaitForSeconds(timeForDisable);
            foreach (Collider2D c in GetComponents<Collider2D>()) {
                c.enabled = false;
            }
        }
    }
    /// <summary>
    /// Función que se ejecuta en cada fotograma.
    /// </summary>
    private void Update()
    {
        audioControl.volume = estadoJuego.Efectos;
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        if (stateInfo.IsName(destroyState)&&stateInfo.normalizedTime>=1) {
            saveManager.DestroyableDestruido(gameObject.name);
            Destroy(gameObject);
        }
    }
}
