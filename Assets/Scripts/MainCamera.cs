﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que gestiona la Camara Principal
/// </summary>
public class MainCamera : MonoBehaviour
{
    /// <summary>
    /// Funcion que se encarga de confinar la camara en un GameObject pasado por parametro
    /// </summary>
    /// <param name="bg">El GameObject al que se confina la camara</param>
    public void setBound(GameObject bg) {
        CinemachineConfiner CinemachineC=GetComponent<CinemachineConfiner>();
        if (bg != null) CinemachineC.m_BoundingShape2D = bg.GetComponent<Collider2D>();
        else CinemachineC.m_BoundingShape2D = null;
    }

  
}
