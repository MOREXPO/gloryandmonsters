﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Clase que gestiona la pausa del juego
/// </summary>
public class Pause : MonoBehaviour
{
    /// <summary>
    /// Indica si la pausa esta activada
    /// </summary>
    bool active;
    /// <summary>
    /// Objeto de tipo <see cref="Canvas"/>
    /// </summary>
    Canvas canvas;
    /// <summary>
    /// Array de objetos de tipo <see cref="GameObject"/>
    /// </summary>
    public GameObject[] objectsPause;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if (active)
        {
            for (int i = 0; i < objectsPause.Length; i++)
            {
                objectsPause[i].SetActive(true);
            }
        }
        else {
            for (int i = 0; i < objectsPause.Length; i++)
            {
                objectsPause[i].SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            active = !active;
            canvas.enabled = active;
            Time.timeScale = (active) ? 0 : 1f; 
        }
    }
}
