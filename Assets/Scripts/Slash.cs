﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja el Slash del ataque magico
/// </summary>
public class Slash : MonoBehaviour {
    /// <summary>
    /// Tiempo en segundos antes de que se destruya el objeto
    /// </summary>
    [Tooltip("Esperar X segundos antes de destruir el objeto")]
    public float waitBeforeDestroy;
    /// <summary>
    /// La posicion del Slash
    /// </summary>
    [HideInInspector]
    public Vector2 mov;
    /// <summary>
    /// La velocidad a la que va el Slash
    /// </summary>
    public float speed;
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update () {
        transform.position += new Vector3(mov.x,mov.y,0) * speed * Time.deltaTime;
    }
    /// <summary>
    /// Funcion que gestiona la colision con algun objeto,hiriendo a los enemigos y destruyendose si choca con algun objeto
    /// </summary>
    /// <param name="col">GameObject contra el que choca</param>
    /// <returns></returns>
    IEnumerator OnTriggerEnter2D (Collider2D col) {
        if (col.tag == "Object") {
            yield return new WaitForSeconds(waitBeforeDestroy);
            Destroy(gameObject);
        } else if (col.tag != "Player" && col.tag != "Attack"){ 
            if (col.tag == "Enemy") col.SendMessage("AtaqueMagico");
            if(col.tag!="Background" && col.tag!="Enemy")Destroy(gameObject);
        }
    }

}