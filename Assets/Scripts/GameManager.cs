﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using TMPro;
using Cinemachine;
using UnityEngine.SceneManagement;
/// <summary>
/// Clase para gestionar funciones
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Nombre del personaje
    /// </summary>
    public string Nombre { set; get; }
    /// <summary>
    /// El timeline del comienzo del juego
    /// </summary>
    [SerializeField]private PlayableDirector secuenciaComienzo;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia Texto
    /// </summary>
    public GameObject Texto;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia a la camara del comienzo
    /// </summary>
    public GameObject CM_Comienzo;
    /// <summary>
    /// Objeto de tipo <see cref="AudioClip"/>
    /// </summary>
    public AudioClip clipMusicaFinal;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    public AudioSource audioControl;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    public AudioSource audioRisaMalvada;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referecia a la UI
    /// </summary>
    public GameObject UI;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia al Dialogo del comienzo
    /// </summary>
    public GameObject DialogueHistory;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia al Player
    /// </summary>
    public GameObject Player;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referecia al Tutorial
    /// </summary>
    public GameObject Tutorial;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia a la pausa
    /// </summary>
    public GameObject Pause;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        saveManager.Cargar();
    }
    /// <summary>
    /// Se encarga de asignar el nombre y comenzar con la cinematica del comienzo
    /// </summary>
    public void IntroducirNombre() {
        Nombre = Texto.GetComponent<TextMeshProUGUI>().text;
        if(Nombre.Trim().Length!=1)secuenciaComienzo.Play();
        
    }
    /// <summary>
    /// Se encarga de cambiar la camara a la camara del Player
    /// </summary>
    public void CambiarCamaraPersonaje() {
        CM_Comienzo.GetComponent<CinemachineVirtualCamera>().Priority = -1;
    }
    /// <summary>
    /// Comienza el audio de risa malvada
    /// </summary>
    public void RisaMalvada() {
        audioRisaMalvada.Play();
    }
    /// <summary>
    /// Pausa la musica
    /// </summary>
    public void PausarMusica() {
        audioControl.Pause();
    }
    /// <summary>
    /// Comienza la musica final
    /// </summary>
    public void ReproducirMusicaFinal() {
        audioControl.clip = clipMusicaFinal;
        audioControl.Play();
    }
    /// <summary>
    /// Se encarga de volver al menu, borrando todo lo guardado
    /// </summary>
    public void IrMenuFinal() {
        saveManager.BorrarTodo();
        PlayerPrefs.SetInt("NuevoJuego",1);
        SceneManager.LoadScene(0);
    }
    /// <summary>
    /// Se encarga de volver al menu, guardando el progreso
    /// </summary>
    public void IrMenu()
    {
        saveManager.Guardar();
        PlayerPrefs.SetInt("NuevoJuego", 0);
        SceneManager.LoadScene(0);
    }
    /// <summary>
    /// Se encarga de saltar la introducción
    /// </summary>
    public void saltarIntro()
    {
        secuenciaComienzo.Pause();
        CambiarCamaraPersonaje();
        UI.SetActive(true);
        DialogueHistory.SetActive(false);
        Player.GetComponent<Player>().enabled = true;
        Tutorial.SetActive(false);
        Pause.SetActive(true);
    }
}
