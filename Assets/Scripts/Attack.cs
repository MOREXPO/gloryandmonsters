﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Maneja la colision del <see cref="GameObject"/> Attack del Player.
/// </summary>
public class Attack : MonoBehaviour {
    /// <summary>
    /// Maneja si colisiona contra un enemigo.
    /// </summary>
    /// <param name="col">Es el <see cref="GameObject"/> contra el que colisiona Attack</param>
    void OnTriggerEnter2D (Collider2D col) {
        
        if (col.tag == "Enemy") col.SendMessage("AtaqueFisico");
    }
}
