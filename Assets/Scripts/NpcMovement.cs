﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que controla el movimiento de los NPCs
/// </summary>
public class NpcMovement : MonoBehaviour
{
    /// <summary>
    /// Indica si el Npc se puede mover o no
    /// </summary>
    public bool inMovement;
    /// <summary>
    /// Indica la velocidad a la que se mueve
    /// </summary>
    public float moveSpeed;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    private Rigidbody2D myRigidBody;
    /// <summary>
    /// Indica si esta en movimiento
    /// </summary>
    public bool isWalking;
    /// <summary>
    /// El tiempo que caminan
    /// </summary>
    public float walkTime;
    /// <summary>
    /// Variable auxiliar de <see cref="walkTime"/>
    /// </summary>
    private float walkCounter;
    /// <summary>
    /// El tiempo que espera antes de caminar
    /// </summary>
    public float waitTime;
    /// <summary>
    /// Variable auxiliar de <see cref="waitTime"/>
    /// </summary>
    private float waitCounter;
    /// <summary>
    /// Controla la dirección en la que camina
    /// </summary>
    private int WalkDirection;
    /// <summary>
    /// Objeto de tipo <see cref="Dialogue_Manager"/> que controla el dialogo
    /// </summary>
    private Dialogue_Manager dialogue_manager;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        waitCounter = waitTime;
        walkCounter = walkTime;
        if (GetComponent<Dialogue_Manager>() != null) dialogue_manager = GetComponent<Dialogue_Manager>();
        if(GetComponent<Animator>()!=null)anim = GetComponent<Animator>();
        
        
        if (inMovement)ChooseDirection();
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if(dialogue_manager != null)if (dialogue_manager.IsWithin()) isWalking = false;
        if (!inMovement) isWalking = false;
        if (isWalking)
        {
            walkCounter -= Time.deltaTime;
            
            switch (WalkDirection) {
                case 0:
                    myRigidBody.velocity = new Vector2(0,moveSpeed);
                    break;
                case 1:
                    myRigidBody.velocity = new Vector2(moveSpeed,0);
                    break;
                case 2:
                    myRigidBody.velocity = new Vector2(0,-moveSpeed);
                    break;
                case 3:
                    myRigidBody.velocity = new Vector2(-moveSpeed, 0);
                    break;
            }
            if (GetComponent<Animator>()!=null) {
                anim.speed = 1;
                anim.SetFloat("movX", myRigidBody.velocity.x);
                anim.SetFloat("movY", myRigidBody.velocity.y);
                anim.SetBool("walking", true);
            }
            

            if (walkCounter < 0)
            {
                isWalking = false;
                waitCounter = waitTime;
            }
        }
        else {
            waitCounter -= Time.deltaTime;
            myRigidBody.velocity = Vector2.zero;
            if (GetComponent<Animator>() != null) anim.SetBool("walking", false);
            if (waitCounter<0) {
                ChooseDirection();
            }
        }
    }
    /// <summary>
    /// Función que realiza el cambio de dirección y empieza a caminar
    /// </summary>
    public void ChooseDirection() {
        WalkDirection = Random.Range(0,4);
        isWalking = true;
        walkCounter = walkTime;
    }
}
