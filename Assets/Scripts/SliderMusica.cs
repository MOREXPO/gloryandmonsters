﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Clase que maneja el Slider de musica
/// </summary>
public class SliderMusica : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="Slider"/>
    /// </summary>
    private Slider slider;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script
    /// </summary>
    void Start()
    {
        slider = GetComponent<Slider>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        slider.value = estadoJuego.Musica;
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        estadoJuego.Musica = slider.value;
    }
}
