﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja al enemigo Troncogro
/// </summary>
public class Enemy_Troncogro : MonoBehaviour {

    /// <summary>
    /// float que indica el radio de la vision del enemigo 
    /// </summary>
    public float visionRadius;
    /// <summary>
    /// float que indica el radio de ataque del enemigo 
    /// </summary>
    public float attackRadius;
    /// <summary>
    /// float que indica la velocidad de movimiento del enemigo
    /// </summary>
    public float speed;
    /// <summary>
    /// Int que indica el nivel del enemigo
    /// </summary>
    public int level;
    /// <summary>
    /// Prefab de la roca
    /// </summary>
    [Tooltip("Prefab de la roca que se disparará")]
    public GameObject rockPrefab;
    /// <summary>
    /// Int que indica la velocidad de ataque del enemigo
    /// </summary>
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public float attackSpeed = 2f;
    /// <summary>
    /// Indica si esta atacando
    /// </summary>
    bool attacking;
    /// <summary>
    /// Puntos de vida maximo
    /// </summary>
    [Tooltip("Puntos de vida")]
    public int maxHp = 3;
    /// <summary>
    /// Puntos de vida actuales
    /// </summary>
    [Tooltip("Vida actual")]
    public int hp;
    /// <summary>
    /// La experiencia que se obtiene por matarlo
    /// </summary>
    public int exp;
    /// <summary>
    /// Objeto de tipo <see cref="Ui"/>
    /// </summary>
    public Ui ui;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que referencia al Player
    /// </summary>
    GameObject player;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objetos del tipo <see cref="Vector3"/> que controlan la posicion del enemigo
    /// </summary>
    Vector3 initialPosition, target;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start () {

       
        player = GameObject.FindGameObjectWithTag("Player");

       
        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        
        hp = maxHp;
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
    }
    /// <summary>
    /// Función que se llama en cada fotograma
    /// </summary>
    void Update () {

        audioControl.volume = estadoJuego.Efectos;
        target = initialPosition;

        
        if (player!=null) {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                player.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")
            
            );

            
            Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);

            
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    target = player.transform.position;
                }
            }
        }
        

        
        float distance = Vector3.Distance(target, transform.position);
        Vector3 dir = (target - transform.position).normalized;

       
        if (target != initialPosition && distance < attackRadius){
           
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.Play("Enemy_Walk", -1, 0); 

            
            if (!attacking) StartCoroutine(Attack(attackSpeed));
        }
        
        else {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);

            
            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }

       
        if (target == initialPosition && distance < 0.05f){
            transform.position = initialPosition; 
           
            anim.SetBool("walking", false);
        }

        
        Debug.DrawLine(transform.position, target, Color.green);
    }

    /// <summary>
    /// Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    /// </summary>
    void OnDrawGizmosSelected() {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }
    /// <summary>
    /// Gestion del taque del enemigo
    /// </summary>
    /// <param name="seconds">Los segundos de retardo entre piedra y piedra</param>
    /// <returns>El retardo entre ataque y ataque</returns>
    IEnumerator Attack(float seconds){
        attacking = true;  
        
        if (target != initialPosition && rockPrefab != null) {
            audioControl.Play();
            Instantiate(rockPrefab, transform.position, transform.rotation);
            yield return new WaitForSeconds(seconds);
        }
        attacking = false; 
    }

    /// <summary>
    /// Gestión del ataque fisico
    /// </summary>
    public void AtaqueFisico(){
        hp -= playerScript.Fuerza;
        if (hp <= 0) {
            saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }
    /// <summary>
    /// Gestión del ataque Magico
    /// </summary>
    public void AtaqueMagico()
    {
        hp -= playerScript.Magia;
        if (hp <= 0)
        {
            saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }

    /// <summary>
    /// Dibujamos las vidas del enemigo en una barra 
    /// </summary>
    void OnGUI() {
        
        Vector2 pos = Camera.main.WorldToScreenPoint (transform.position);
        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y-60,
                40,
                24
            ), "L-" + level
        );

        GUI.Box(
            new Rect(
                pos.x - 20,                   
                Screen.height - pos.y + 60,  
                40,                               
                24                             
            ),hp + "/" + maxHp               
        );
    }

}