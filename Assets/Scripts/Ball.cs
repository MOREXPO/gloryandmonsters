﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja la bola de magia del mago oscuro.
/// </summary>
public class Ball : MonoBehaviour
{
    /// <summary>
    /// Velocidad de la bola.
    /// </summary>
    [Tooltip("Velocidad de movimiento en unidades del mundo")]
    public float speed;
    /// <summary>
    /// Recuperamos al objeto jugador.
    /// </summary>
    GameObject player;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Vectores para almacenar el objetivo y su dirección
    /// </summary>
    Vector3 target, dir;
    /// <summary>
    /// Recuperamos la barra de vida
    /// </summary>
    private GameObject healthbar;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script
    /// </summary>
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb2d = GetComponent<Rigidbody2D>();
        healthbar = GameObject.Find("Healthbar");

        if (player != null)
        {
            target = player.transform.position;
            dir = (target - transform.position).normalized;
        }
    }

    /// <summary>
    /// Si hay un objetivo movemos la roca hacia su posición
    /// </summary>
    void FixedUpdate()
    {

        if (target != Vector3.zero)
        {
            rb2d.MovePosition(transform.position + (dir * speed) * Time.deltaTime);
        }
    }
    /// <summary>
    /// Si chocamos contra el jugador o un ataque la borramos
    /// </summary>
    /// <param name="col">El <see cref="GameObject"/> contra el que choque la bola</param>
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.transform.tag == "Player" || col.transform.tag == "Attack")
        {
            Destroy(gameObject);
            if (col.transform.tag == "Player")
            {
                healthbar.SendMessage("TakeDamage", 100);
            }
        }
    }

    /// <summary>
    /// Si se sale de la pantalla borramos la bola
    /// </summary>
    void OnBecameInvisible()
    {

        Destroy(gameObject);
    }
}
