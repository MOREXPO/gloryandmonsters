﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
/// <summary>
/// Clase que gestiona el saltar la introducción
/// </summary>
public class SaltarIntro : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="GameManager"/>
    /// </summary>
    private GameManager gameManager;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script
    /// </summary>
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            gameManager.saltarIntro();
            gameObject.SetActive(false);
        }
    }

    
}
