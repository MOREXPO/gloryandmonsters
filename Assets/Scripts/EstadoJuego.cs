﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Clase que gestiona la Musica y Efectos
/// </summary>
public class EstadoJuego : MonoBehaviour
{
    /// <summary>
    /// Variable estatica para referenciarnos a la propia clase
    /// </summary>
    public static EstadoJuego estadoJuego;
    /// <summary>
    /// Variable que gestiona la musica
    /// </summary>
    public float Musica { set; get; }
    /// <summary>
    /// Variable que gestiona los efectos
    /// </summary>
    public float Efectos { set; get; }
    /// <summary>
    /// Funcion que se llama al cargar la instancia del script
    /// </summary>
    private void Awake()
    {
        if (estadoJuego == null)
        {
            Musica = PlayerPrefs.GetFloat("Musica",0.5f);
            Efectos = PlayerPrefs.GetFloat("Efectos",0.5f);
            estadoJuego = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(estadoJuego!=this){
            Destroy(gameObject);
        }
        
    }
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {

    }
    /// <summary>
    /// Sirve para guardar la musica y los efectos con PlayerPrefs
    /// </summary>
    public void GuardarMusicaEfectos() {
        PlayerPrefs.SetFloat("Musica",Musica);
        PlayerPrefs.SetFloat("Efectos",Efectos);
    }
}
