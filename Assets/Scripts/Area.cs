﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Maneja los componentes del GameObject <c>Area</c>.
/// </summary>
public class Area : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource musica;
    /// <summary>
    /// Objeto de tipo <see cref="GameManager"/>
    /// </summary>
    private GameManager gameManager;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        anim = GetComponent<Animator>();
        musica = GameObject.Find("MusicaAmbiente").GetComponent<AudioSource>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    /// <summary>
    /// Esta función cambia el texto en los hijos del GameObject Area con una animación de transición.
    /// </summary>
    /// <param name="name">El texto a cambiar</param>
    /// <returns>Devuelve un retardo de tiempo</returns>
    public IEnumerator ShowArea(string name) {
        anim.Play("Area_Show");
        transform.GetChild(0).GetComponent<Text>().text = name;
        transform.GetChild(1).GetComponent<Text>().text = name;
        yield return new WaitForSeconds(1f);
        anim.Play("Area_FadeOut");
        if (name == "GAME OVER") {
            musica.Pause();
            yield return new WaitForSeconds(2f);
            gameManager.IrMenuFinal(); 
        }
    }
}
