﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja al enemigo a melee
/// </summary>
public class Enemy_Melee : MonoBehaviour
{

    /// <summary>
    /// float que indica el radio de la vision del enemigo 
    /// </summary>
    public float visionRadius;
    /// <summary>
    /// float que indica el radio de ataque del enemigo 
    /// </summary>
    public float attackRadius;
    /// <summary>
    /// float que indica la velocidad de movimiento del enemigo
    /// </summary>
    public float speed;
    /// <summary>
    /// Int que indica el nivel del enemigo
    /// </summary>
    public int level;
    /// <summary>
    /// Int que indica la velocidad de ataque del enemigo
    /// </summary>
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public int attackSpeed = 1000;
    /// <summary>
    /// Collider del ataque del enemigo
    /// </summary>
    CircleCollider2D attackCollider;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Indica si esta atacando
    /// </summary>
    bool attacking;
    /// <summary>
    /// Puntos de vida maximo
    /// </summary>
    [Tooltip("Puntos de vida")]
    public int maxHp = 3;
    /// <summary>
    /// Puntos de vida actuales
    /// </summary>
    [Tooltip("Vida actual")]
    public int hp;
    /// <summary>
    /// La experiencia que se obtiene por matarlo
    /// </summary>
    public int exp;
    /// <summary>
    /// Objeto de tipo <see cref="Ui"/>
    /// </summary>
    public Ui ui;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que referencia al Player
    /// </summary>
    GameObject player;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objetos del tipo <see cref="Vector3"/> que controlan la posicion del enemigo
    /// </summary>
    Vector3 initialPosition, target, dir;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Floats que manejan el tiempo en el que ataca
    /// </summary>
    float timer, waitTime;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        attacking = false;
        waitTime = attackSpeed;
        player = GameObject.FindGameObjectWithTag("Player");


        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        hp = maxHp;

        attackCollider = transform.GetChild(0).GetComponent<CircleCollider2D>();
        attackCollider.enabled = false;
        timer = waitTime;
        Time.timeScale = 1;
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        
    }
    /// <summary>
    /// Función que se llama en cada fotograma
    /// </summary>
    void Update()
    {

        audioControl.volume = estadoJuego.Efectos;
        target = initialPosition;


        if (player != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                player.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")

            );


            Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);


            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    target = player.transform.position;
                }
            }
        }



        float distance = Vector3.Distance(target, transform.position);
        dir = (target - transform.position).normalized;


        if (target != initialPosition && distance < attackRadius)
        {
            Ataque();
            if (!attacking) anim.SetBool("walking", false);
        }

        else
        {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);


            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }


        if (target == initialPosition && distance < 0.05f)
        {
            transform.position = initialPosition;
            anim.SetBool("walking", false);
        }


        Debug.DrawLine(transform.position, target, Color.green);
    }

    /// <summary>
    /// Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    /// </summary>
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }

    /// <summary>
    /// Gestión del ataque fisico
    /// </summary>
    public void AtaqueFisico()
    {
        hp -= playerScript.Fuerza;
        if (hp <= 0)
        {
            if (!name.StartsWith("Zombie")&&!name.StartsWith("Esqueleto")) saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }
    /// <summary>
    /// Gestión del ataque Magico
    /// </summary>
    public void AtaqueMagico()
    {
        hp -= playerScript.Magia;
        if (hp <= 0)
        {
            if (!name.StartsWith("Zombie") && !name.StartsWith("Esqueleto")) saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }

    /// <summary>
    /// Dibujamos las vidas del enemigo en una barra 
    /// </summary>
    void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y - 60,
                40,
                24
            ), "L-" + level
        );

        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y + 60,
                60,
                24
            ), hp + "/" + maxHp
        );
    }
    /// <summary>
    /// Gestiona el ataque del enemigo
    /// </summary>
    void Ataque()
    {
        print("Ataque");
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        if(name.StartsWith("Goblin"))attacking = stateInfo.IsName("Goblin_Attack");
        else if(name.StartsWith("Golem")) attacking = stateInfo.IsName("Golem_Attack");
        else if (name.StartsWith("Zombie")) attacking = stateInfo.IsName("Zombie_Attack");
        else if (name.StartsWith("Esqueleto")) attacking = stateInfo.IsName("Esqueleto_Attack");
        if (!attacking)
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                anim.SetTrigger("attacking");
                timer = timer - waitTime;
                if (audioControl != null) audioControl.Play();
            }

        }

        if (dir != Vector3.zero) attackCollider.offset = new Vector2(dir.x / 2, dir.y / 2);

        if (attacking)
        {
            float playbackTime = stateInfo.normalizedTime;
            if (playbackTime > 0.33 && playbackTime < 0.66) attackCollider.enabled = true;
            else attackCollider.enabled = false;
        }
    }
}
