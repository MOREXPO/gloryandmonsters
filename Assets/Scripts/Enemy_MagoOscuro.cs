﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
/// <summary>
/// Clase que maneja al enemigo mago oscuro
/// </summary>
public class Enemy_MagoOscuro : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="PlayableDirector"/>
    /// </summary>
    [SerializeField] private PlayableDirector secuenciaCreditos;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/>
    /// </summary>
    public GameObject Creditos;
    /// <summary>
    /// float que indica el radio de la vision del enemigo
    /// </summary>
    public float visionRadius;
    /// <summary>
    /// float que indica el radio de ataque del enemigo
    /// </summary>
    public float attackRadius;
    /// <summary>
    /// float que indica la velocidad del enemigo
    /// </summary>
    public float speed;
    /// <summary>
    /// float que indica el radio de invocación del enemigo
    /// </summary>
    public float radioInvocacion = 2f;
    /// <summary>
    /// Int que indica el nivel del enemigo
    /// </summary>
    public int level;
    /// <summary>
    /// Array de los GameObject de los teletransportes
    /// </summary>
    public GameObject[] tps;
    /// <summary>
    /// bool para indicar si puede invocar
    /// </summary>
    bool invocar = false;
    /// <summary>
    /// Int para que tenga aleateoridad en la invocacion de un zombie o un esqueleto
    /// </summary>
    int random;
    /// <summary>
    /// Int que indica la velocidad de ataque en segundos.
    /// </summary>
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public int attackSpeed = 1000;
    /// <summary>
    /// Bool para indicar si esta atacando
    /// </summary>
    bool attacking;
    /// <summary>
    /// Prefab de la bola magica
    /// </summary>
    [Tooltip("Prefab de la bola que se disparará")]
    public GameObject ballPrefab;
    /// <summary>
    /// Prefab del zombie que invoca
    /// </summary>
    public GameObject zombiePrefab;
    /// <summary>
    /// Prefab del esqueleto que invoca
    /// </summary>
    public GameObject esqueletoPrefab;
    /// <summary>
    /// Prefab que se usara para manejar los otros prefabs
    /// </summary>
    GameObject prefabRandom;
    /// <summary>
    /// Int que indica los puntos de vida maxima
    /// </summary>
    [Tooltip("Puntos de vida")]
    public int maxHp = 3;
    /// <summary>
    /// Int que indica los puntos de vida que tiene
    /// </summary>
    [Tooltip("Vida actual")]
    public int hp;
    /// <summary>
    /// Int que indica la experiencia que obtines por matarlo
    /// </summary>
    public int exp;
    /// <summary>
    /// Objeto de tipo <see cref="Ui"/>
    /// </summary>
    public Ui ui;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia al Player
    /// </summary>
    GameObject player;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Objetos del tipo <see cref="Vector3"/> que controlan la posicion del enemigo
    /// </summary>
    Vector3 initialPosition, target, dir;
    /// <summary>
    /// Sonido del audio cuando se invoca un monstruo
    /// </summary>
    public AudioClip audioInvocacion;
    /// <summary>
    /// Sonido del audio cuando lanza una bola magica
    /// </summary>
    public AudioClip audioHechizo;
    /// <summary>
    /// Sonido del audio cuando se teletransporta
    /// </summary>
    public AudioClip audioTeleport;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Floats que manejan el tiempo en el que ataca
    /// </summary>
    float timer, waitTime;
    /// <summary>
    /// Floats que manejan el tiempo en el que invoca algun monstruo
    /// </summary>
    float timerInvocacion, waitTimeInvocacion;
    /// <summary>
    /// Floats que manejan el tiempo en el que se teletransporta
    /// </summary>
    float timerTp, waitTimeTp;
    /// <summary>
    /// Array de GameObjects de los enemigos
    /// </summary>
    private GameObject[] enemigos;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        attacking = false;
        waitTime = attackSpeed;
        waitTimeInvocacion = 0.5f;
        waitTimeTp = 6;
        player = GameObject.FindGameObjectWithTag("Player");
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        hp = maxHp;
        timer = waitTime;
        Time.timeScale = 1;
    }
    /// <summary>
    /// Función que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        audioControl.volume = estadoJuego.Efectos;

        target = initialPosition;


        if (player != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                player.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")

            );


            Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);


            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    target = player.transform.position;
                }
            }
        }



        float distance = Vector3.Distance(target, transform.position);
        dir = (target - transform.position).normalized;


        if (target != initialPosition && distance < attackRadius)
        {
            timerTp += Time.deltaTime;
            if (timerTp > waitTimeTp)
            {
                audioControl.clip = audioTeleport;
                audioControl.Play();
                transform.position = tps[Random.Range(0,tps.Length)].transform.position;
                timerTp = timerTp - waitTimeTp;
            }
            Ataque();
            if (!attacking) anim.SetBool("walking", false);
        }

        else
        {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);


            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }


        if (target == initialPosition && distance < 0.05f)
        {
            transform.position = initialPosition;
            anim.SetBool("walking", false);
        }


        Debug.DrawLine(transform.position, target, Color.green);
    }
    /// <summary>
    /// Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    /// </summary>

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }
    /// <summary>
    /// Gestión del ataque fisico
    /// </summary>
    public void AtaqueFisico()
    {
        hp -= playerScript.Fuerza;
        if (hp <= 0)
        {
            ActivarCreditos();
        }
    }
    /// <summary>
    /// Gestión del ataque Magico
    /// </summary>
    public void AtaqueMagico()
    {
        hp -= playerScript.Magia;
        if (hp <= 0)
        {
            ActivarCreditos();
        }
    }
    /// <summary>
    /// Destruye el mago oscuro y inicia la secuencia de creditos.
    /// </summary>
    void ActivarCreditos() {
        Creditos.SetActive(true);
        secuenciaCreditos.Play();
        enemigos=GameObject.FindGameObjectsWithTag("Enemy");
        for (int i=enemigos.Length-1;i>=0;i--) {
            Destroy(enemigos[i]);
        }
        saveManager.BorrarTodo();
        PlayerPrefs.SetInt("NuevoJuego", 1);
        Destroy(gameObject);
    }
    /// <summary>
    /// Dibujamos las vidas del enemigo en una barra  
    /// </summary>
    void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y - 60,
                40,
                24
            ), "L-" + level
        );

        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y + 60,
                60,
                24
            ), hp + "/" + maxHp
        );
    }
    /// <summary>
    /// Gestiona el ataque del enemigo
    /// </summary>
    void Ataque()
    {
        
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        attacking = stateInfo.IsName("MagoOscuro_Attack");
        if (!attacking)
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                anim.SetTrigger("attacking");
                timer = timer - waitTime;
            }

        }


        if (attacking)
        {
            

            timerInvocacion += Time.deltaTime;
            if (timerInvocacion > waitTimeInvocacion)
            {
                if (invocar)
                {
                    audioControl.clip = audioInvocacion;
                    audioControl.Play();
                    random = Random.Range(1, 3);
                    if (random == 1)
                    {
                        print("zombie");
                        prefabRandom = zombiePrefab;
                        Instantiate(prefabRandom, transform.position + Random.onUnitSphere * radioInvocacion, Quaternion.identity);
                    }
                    else
                    {
                        print("esqueleto");
                        prefabRandom = esqueletoPrefab;
                        Instantiate(prefabRandom, transform.position + Random.onUnitSphere * radioInvocacion, Quaternion.identity);
                    }
                    
                }
                else {
                    print("bola");
                    prefabRandom = ballPrefab;
                    audioControl.clip = audioHechizo;
                    audioControl.Play();
                    Instantiate(prefabRandom, transform.position, Quaternion.identity);
                }

                invocar = !invocar;
                timerInvocacion = timerInvocacion - waitTimeInvocacion;
            }

        }
    }
}
