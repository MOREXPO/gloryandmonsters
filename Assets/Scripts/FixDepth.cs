﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que gestiona las profundidades de los objetos
/// </summary>
public class FixDepth : MonoBehaviour
{
    /// <summary>
    /// Indica si esta en funcionamiento todo el rato
    /// </summary>
    public bool fixEveryFrame;
    /// <summary>
    /// Componente <see cref="SpriteRenderer"/>
    /// </summary>
    SpriteRenderer spr;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        spr.sortingLayerName = "Player";
        spr.sortingOrder = Mathf.RoundToInt(-transform.position.y*100);
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if (fixEveryFrame) {
            spr.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100);
        }
    }
}
