﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;
/// <summary>
/// Clase que maneja la vida del Player
/// </summary>
public class Healthbar : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Image"/> que hace referecia a la imagen de la vida
    /// </summary>
    public Image health;
    /// <summary>
    /// Float que indica la vida del Player
    /// </summary>
    public float Hp { set; get; }
    /// <summary>
    /// Float que indica la vida maxima del Player
    /// </summary>
    float maxHp;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referecia al Area
    /// </summary>
    GameObject area;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referecia al Player
    /// </summary>
    public GameObject player;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        maxHp = playerScript.Vida;
        Hp = PlayerPrefs.GetFloat("hp",maxHp);
        area = GameObject.FindGameObjectWithTag("Area");
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        maxHp = playerScript.Vida;
        if (Hp<=0) {
            Destroy(player);
            StartCoroutine(area.GetComponent<Area>().ShowArea("GAME OVER"));
        }
        health.transform.localScale = new Vector2(Hp / maxHp, 1);
    }
    /// <summary>
    /// Funcion que se encarga de gestionar el daño recibido
    /// </summary>
    /// <param name="amount">El daño recibido</param>
    public void TakeDamage(float amount) {
        if (playerScript.Resistencia <= amount)
        {
            Hp = Mathf.Clamp(Hp + playerScript.Resistencia - amount, 0f, maxHp);
        }
    }
    /// <summary>
    /// Funcion que se encarga de curar la vida
    /// </summary>
    /// <param name="cura">La cantidad que se cura</param>
    public void Curar(float cura) {
        Hp = Mathf.Clamp(Hp+cura,0f,maxHp);
    }
    /// <summary>
    /// Funcion que se encarga de curar toda la vida
    /// </summary>
    public void CurarTodo() {
        Hp = maxHp + 15;
    }
}
