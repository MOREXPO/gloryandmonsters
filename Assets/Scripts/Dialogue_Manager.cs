﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
/// <summary>
/// Clase que maneja los dialogos entre los personajes
/// </summary>
public class Dialogue_Manager : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Dialogue"/>
    /// </summary>
    public Dialogue dialogue;
    /// <summary>
    /// Objeto de tipo <see cref="Queue"/>
    /// </summary>
    Queue<string> sentences;
    /// <summary>
    /// Objeto de tipo <see cref="dialoguePanel"/>
    /// </summary>
    public GameObject dialoguePanel;
    /// <summary>
    /// Objeto de tipo <see cref="TextMeshProUGUI"/>
    /// </summary>
    public TextMeshProUGUI displayText;
    /// <summary>
    /// Objeto de tipo <see cref="GameManager"/>
    /// </summary>
    public GameManager gameManager;
    /// <summary>
    /// Objeto de tipo <see cref="bool"/> que indica si el personaje esta dentro del collider
    /// </summary>
    bool isWithin;
    /// <summary>
    /// Objeto de tipo <see cref="string"/>
    /// </summary>
    string activeSentence;
    /// <summary>
    /// Objeto de tipo <see cref="float"/> que indica la velocidad a la que se escriben las letras
    /// </summary>
    public float typingSpeed;
    /// <summary>
    /// Objeto de tipo <see cref="myAudio"/>
    /// </summary>
    AudioSource myAudio;
    /// <summary>
    /// Objeto de tipo <see cref="AudioClip"/>
    /// </summary>
    public AudioClip speakSound;
    /// <summary>
    /// Objeto de tipo <see cref="bool"/>
    /// </summary>
    public bool isHistory;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Funcion que se llama al cargar la instancia del script
    /// </summary>
    private void Awake()
    {
        isWithin = false;
    }
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        sentences = new Queue<string>();
        myAudio = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        myAudio.volume = estadoJuego.Efectos;
        if (isWithin&&!isHistory) {
            if (Input.GetKeyDown(KeyCode.Return)&&displayText.text.Replace(gameManager.Nombre,"{nombre}")==activeSentence)
            {
                DisplayNextSentence();
            }
        }    
    }
    /// <summary>
    /// Cuando el Player choca contra el collider se activa el dialogo
    /// </summary>
    /// <param name="collision">El GameObject que choque contra el collider</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Player")) {
            dialoguePanel.SetActive(true);
            isWithin = true;
            StartDialogue();
        }
    }
    /// <summary>
    /// Cuando el Player choca contra el collider se desactiva el dialogo
    /// </summary>
    /// <param name="collision">El GameObject que choque contra el collider</param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isWithin = false;
            dialoguePanel.SetActive(false);
            StopAllCoroutines();
        }
    }
    /// <summary>
    /// Inicia el dialogo
    /// </summary>
    void StartDialogue() {
        sentences.Clear();
        foreach (string sentence in dialogue.sentenceList) {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }
    /// <summary>
    /// Pasa a la siguiente frase si existe
    /// </summary>
    public void DisplayNextSentence() {
        if (sentences.Count<=0) {
            displayText.text = activeSentence;
            return;
        }
        activeSentence = sentences.Dequeue();
        displayText.text = activeSentence;
        StopAllCoroutines();
        StartCoroutine(TypeTheSentence(activeSentence));
    }
    /// <summary>
    /// Funcion que escribe la frase letra a letra cada cierto segundo.
    /// </summary>
    /// <param name="sentence">La frase a escribir</param>
    /// <returns>El retardo de tiempo entre letra y letra</returns>
    IEnumerator TypeTheSentence(string sentence) {
        displayText.text = "";
        sentence = sentence.Replace("{nombre}",gameManager.Nombre);
        foreach (char letter in sentence.ToCharArray()) {
            displayText.text += letter;
            myAudio.PlayOneShot(speakSound);
            yield return new WaitForSeconds(typingSpeed);
        }
    }
    /// <summary>
    /// Función que devuelve si esta dentro el Player
    /// </summary>
    /// <returns>Devuelve si esta dentro el Player</returns>
    public bool IsWithin() {
        return isWithin;
    }
}
