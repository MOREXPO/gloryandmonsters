﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Cinemachine;
/// <summary>
/// Clase que gestiona los Warps
/// </summary>
public class Warp : MonoBehaviour
{
    /// <summary>
    /// Es el warp al que se teletransporta
    /// </summary>
    public GameObject target;
    /// <summary>
    /// Es el escenario al que se teletransporta
    /// </summary>
    public GameObject targetMap;
    /// <summary>
    /// Es el Background del escenario al que se teletransporta
    /// </summary>
    public GameObject targetBackground;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/> que hace referecia a la musica ambiente
    /// </summary>
    public AudioSource musicaAmbiente;
    /// <summary>
    /// Clip que se pondra al teletransportarse
    /// </summary>
    public AudioClip audioClip;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// indica el comienzo de la animación de teletransportación
    /// </summary>
    bool start = false;
    /// <summary>
    /// Indica si aparece o desaparece
    /// </summary>
    bool isFadeIn = false;
    /// <summary>
    /// Indica el nivel de opacidad
    /// </summary>
    float alpha = 0;
    /// <summary>
    /// Tiempo en segundos que dura la animación
    /// </summary>
    float fadeTime = 1f;
    /// <summary>
    /// Objetos de tipo <see cref="GameObject"/>
    /// </summary>
    GameObject area,CamaraPlayer;
    /// <summary>
    /// Funcion que se llama al cargar la instancia del script
    /// </summary>
    private void Awake()
    {
        Assert.IsNotNull(target);
        GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        Assert.IsNotNull(targetMap);
        area = GameObject.FindGameObjectWithTag("Area");
        CamaraPlayer = GameObject.Find("CM_Player");
    }
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    private void Update()
    {
        audioControl.volume = estadoJuego.Efectos;
    }
    /// <summary>
    /// Maneja si el Player colisiona con el Warp para teletransportarlo
    /// </summary>
    /// <param name="collision">El GameObject que colisiona contra el Warp</param>
    /// <returns>El retado de tiempo que tarda en teletransportarlo</returns>
    private IEnumerator OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Animator>().enabled = false;
            collision.GetComponent<Player>().enabled = false;
            FadeIn();
            audioControl.Play();
            yield return new WaitForSeconds(fadeTime);
            if (audioClip != null)
            {
                musicaAmbiente.clip = audioClip;
                musicaAmbiente.Play();
            }
            collision.transform.position = target.transform.GetChild(0).transform.position;
            CamaraPlayer.GetComponent<MainCamera>().setBound(targetBackground);
            
            FadeOut();
            collision.GetComponent<Animator>().enabled = true;
            collision.GetComponent<Player>().enabled = true;

            StartCoroutine(area.GetComponent<Area>().ShowArea(targetMap.tag));
            if (targetBackground != null)
            {
                PlayerPrefs.SetString("escenarioActual", targetBackground.name);
                if (targetBackground.name == "Background_Escenario1")
                {
                    GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 4;
                }
                else {
                    GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 6;
                }
                

            }
            else {
                PlayerPrefs.SetString("escenarioActual", "null");
                GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 4;
            }
            
            if (audioClip!=null) {
                PlayerPrefs.SetString("WarpActual", gameObject.name);
            }  
        }
    }
    /// <summary>
    /// Adminstra eventos de GUI
    /// </summary>
    private void OnGUI()
    {
        if (!start) return;
        GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b,alpha);
        Texture2D tex;
        tex = new Texture2D(1,1);
        tex.SetPixel(0,0,Color.black);
        tex.Apply();
        GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),tex);
        if (isFadeIn)
        {
            alpha = Mathf.Lerp(alpha, 1.1f, fadeTime * Time.deltaTime);
        }
        else {
            alpha = Mathf.Lerp(alpha, -0.1f, fadeTime * Time.deltaTime);
            if (alpha < 0) start = false;
        }
    }
    /// <summary>
    /// Función para indicar que aparece
    /// </summary>
    void FadeIn() {
        start = true;
        isFadeIn = true;
    }
    /// <summary>
    /// Función para indicar que desaparece
    /// </summary>
    void FadeOut() {
        isFadeIn = false;
    }
}
