﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Maneja la colision del <see cref="GameObject"/> Attack del Enemigo.
/// </summary>
public class Attack_Enemy : MonoBehaviour
{
    /// <summary>
    /// El daño que le va a hacer al Player.
    /// </summary>
    public int fuerza;
    /// <summary>
    /// El <see cref="GameObject"/ de la barra de vida>
    /// </summary>
    public GameObject healthbar;
    /// <summary>
    /// Maneja si colisiona contra el Player.
    /// </summary>
    /// <param name="col">Es el <see cref="GameObject"/> contra el que colisiona Attack_Enemy</param>
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") healthbar.SendMessage("TakeDamage", fuerza);
    }
}
