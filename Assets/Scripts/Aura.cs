﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Maneja la animacion del aura del ataque magico.
/// </summary>
public class Aura : MonoBehaviour {

    /// <summary>
    /// El tiempo en segundos para que aparezca el aura.
    /// </summary>
    public float waitBeforePlay;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Objeto de tipo <see cref="Coroutine"/>
    /// </summary>
    Coroutine manager;
    /// <summary>
    /// Objeto de tipo <see cref="bool"/>
    /// </summary>
    bool loaded;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start () {
        anim = GetComponent<Animator>();
    }
    /// <summary>
    /// Se inicia a cargar el ataque magico pero sin que aparezca la animacion del aura.
    /// </summary>
    public void AuraStart() {
        manager = StartCoroutine(Manager());
        anim.Play("Aura_Idle");
    }
    /// <summary>
    /// Se ejecuta una vez se deje de pulsar el ataque magico.
    /// </summary>
    public void AuraStop() {
        StopCoroutine(manager);
        anim.Play("Aura_Idle");
        loaded = false;
    }

    /// <summary>
    /// Inicia la animación del aura.
    /// </summary>
    /// <returns>El retardo de tiempo antes de que se incie la animación del aura</returns>
    public IEnumerator Manager() {
        yield return new WaitForSeconds(waitBeforePlay);
        anim.Play("Aura_Play");
        loaded = true;
    }

    /// <summary>
    /// Devuelve el valor de <see cref="loaded"/>.
    /// </summary>
    /// <returns>El valor de <see cref="loaded"/></returns>
    public bool IsLoaded() {
        return loaded;
    }
}