﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que hace que el objeto persiga a un objetivo
/// </summary>
public class Perseguir : MonoBehaviour
{
    /// <summary>
    /// float que indica el radio de la vision del perseguidor 
    /// </summary>
    public float visionRadius;
    /// <summary>
    /// float que indica el radio de espacio del perseguidor 
    /// </summary>
    public float spaceRadius;
    /// <summary>
    /// float que indica la velocidad de movimiento del perseguidor
    /// </summary>
    public float speed;
    /// <summary>
    /// nombre del GameObject a perseguir
    /// </summary>
    public string objetivo;
    /// <summary>
    /// nombre del Estado Caminar del perseguidor
    /// </summary>
    public string animWalk;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/>
    /// </summary>
    GameObject objectObjetivo;
    /// <summary>
    /// Objetos del tipo <see cref="Vector3"/> que controlan la posicion del perseguidor
    /// </summary>
    Vector3 initialPosition, target;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {


        objectObjetivo = GameObject.FindGameObjectWithTag(objetivo);


        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }
    /// <summary>
    /// Función que se llama en cada fotograma
    /// </summary>
    void Update()
    {


        target = initialPosition;


        if (objectObjetivo != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                objectObjetivo.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")

            );


            Vector3 forward = transform.TransformDirection(objectObjetivo.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);


            if (hit.collider != null)
            {
                if (hit.collider.tag == objetivo)
                {
                    target = objectObjetivo.transform.position;
                }
            }
        }



        float distance = Vector3.Distance(target, transform.position);
        Vector3 dir = (target - transform.position).normalized;


        if (target != initialPosition && distance < spaceRadius)
        {

            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.Play(animWalk, -1, 0);

        }

        else
        {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);


            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }


        if (target == initialPosition && distance < 0.05f)
        {
            transform.position = initialPosition;

            anim.SetBool("walking", false);
        }


        Debug.DrawLine(transform.position, target, Color.green);
    }

    /// <summary>
    /// Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    /// </summary>
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, spaceRadius);

    }
   
}
