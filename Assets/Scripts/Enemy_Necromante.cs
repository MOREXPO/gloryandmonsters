﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que maneja al enemigo Necromante
/// </summary>
public class Enemy_Necromante : MonoBehaviour
{
    /// <summary>
    /// float que indica el radio de la vision del enemigo 
    /// </summary>
    public float visionRadius;
    /// <summary>
    /// float que indica el radio de ataque del enemigo 
    /// </summary>
    public float attackRadius;
    /// <summary>
    /// float que indica la velocidad de movimiento del enemigo
    /// </summary>
    public float speed;
    /// <summary>
    /// float que indica el radio de invocación del enemigo
    /// </summary>
    public float radioInvocacion=2f;
    /// <summary>
    /// Int que indica el nivel del enemigo
    /// </summary>
    public int level;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Int que indica la velocidad de ataque del enemigo
    /// </summary>
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public int attackSpeed = 1000;
    /// <summary>
    /// Indica si esta atacando
    /// </summary>
    bool attacking;
    /// <summary>
    /// Prefab del zombie que invoca
    /// </summary>
    public GameObject zombiePrefab;
    /// <summary>
    /// Prefab del esqueleto que invoca
    /// </summary>
    public GameObject esqueletoPrefab;
    /// <summary>
    /// Prefab que se usara para manejar los otros prefabs
    /// </summary>
    GameObject prefabRandom;
    /// <summary>
    /// Puntos de vida maximo
    /// </summary>
    [Tooltip("Puntos de vida")]
    public int maxHp = 3;
    /// <summary>
    /// Puntos de vida actuales
    /// </summary>
    [Tooltip("Vida actual")]
    public int hp;
    /// <summary>
    /// La experiencia que se obtiene por matarlo
    /// </summary>
    public int exp;
    /// <summary>
    /// Objeto de tipo <see cref="Ui"/>
    /// </summary>
    public Ui ui;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia al Player
    /// </summary>
    GameObject player;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objetos del tipo <see cref="Vector3"/> que controlan la posicion del enemigo
    /// </summary>
    Vector3 initialPosition, target, dir;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// Floats que manejan el tiempo en el que ataca
    /// </summary>
    float timer, waitTime;
    /// <summary>
    /// Floats que manejan el tiempo en el que invoca
    /// </summary>
    float timerInvocacion, waitTimeInvocacion;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        attacking = false;
        waitTime = attackSpeed;
        waitTimeInvocacion = 1;
        player = GameObject.FindGameObjectWithTag("Player");


        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        hp = maxHp;
        timer = waitTime;
        Time.timeScale = 1;
        audioControl = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
    }
    /// <summary>
    /// Función que se llama en cada fotograma
    /// </summary>
    void Update()
    {

        audioControl.volume = estadoJuego.Efectos;
        target = initialPosition;


        if (player != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                player.transform.position - transform.position,
                visionRadius,
                1 << LayerMask.NameToLayer("Default")

            );


            Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
            Debug.DrawRay(transform.position, forward, Color.red);


            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    target = player.transform.position;
                }
            }
        }



        float distance = Vector3.Distance(target, transform.position);
        dir = (target - transform.position).normalized;


        if (target != initialPosition && distance < attackRadius)
        {
            Ataque();
            if (!attacking) anim.SetBool("walking", false);
        }

        else
        {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);


            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }


        if (target == initialPosition && distance < 0.05f)
        {
            transform.position = initialPosition;
            anim.SetBool("walking", false);
        }


        Debug.DrawLine(transform.position, target, Color.green);
    }

    /// <summary>
    /// Podemos dibujar el radio de visión y ataque sobre la escena dibujando una esfera
    /// </summary>
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }

    /// <summary>
    /// Gestión del ataque fisico
    /// </summary>
    public void AtaqueFisico()
    {
        hp -= playerScript.Fuerza;
        if (hp <= 0)
        {
            saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }
    /// <summary>
    /// Gestión del ataque Magico
    /// </summary>
    public void AtaqueMagico()
    {
        hp -= playerScript.Magia;
        if (hp <= 0)
        {
            saveManager.EnemyMuerto(gameObject.name);
            Destroy(gameObject);
            ui.Aumentar(exp);
        }
    }

    /// <summary>
    /// Dibujamos las vidas del enemigo en una barra 
    /// </summary>
    void OnGUI()
    {

        Vector2 pos = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y - 60,
                40,
                24
            ), "L-" + level
        );

        GUI.Box(
            new Rect(
                pos.x - 20,
                Screen.height - pos.y + 60,
                60,
                24
            ), hp + "/" + maxHp
        );
    }
    /// <summary>
    /// Gestiona el ataque del enemigo
    /// </summary>
    void Ataque()
    {
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        attacking = stateInfo.IsName("Necromante_Attack");
        if (!attacking)
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                anim.SetTrigger("attacking");
                timer = timer - waitTime;
                audioControl.Play();
            }

        }


        if (attacking)
        {
            timerInvocacion += Time.deltaTime;
            if (timerInvocacion > waitTimeInvocacion)
            {
                if (Random.Range(1, 3) == 1)
                {
                    prefabRandom = zombiePrefab;
                }
                else {
                    prefabRandom = esqueletoPrefab;
                }
                Instantiate(prefabRandom, transform.position+Random.onUnitSphere*radioInvocacion, Quaternion.identity);
                timerInvocacion = timerInvocacion - waitTimeInvocacion;
            }
            
        }
    }
}
