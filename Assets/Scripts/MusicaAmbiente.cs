﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que controla la Musica
/// </summary>
public class MusicaAmbiente : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    AudioSource controlMusica;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        controlMusica = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        controlMusica.volume = estadoJuego.Musica;
    }
}
