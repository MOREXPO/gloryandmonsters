﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que contiene los datos necesarios para la clase <see cref="Dialogue_Manager"/>
/// </summary>
[System.Serializable]
public class Dialogue
{
    /// <summary>
    /// El nombre del personaje
    /// </summary>
    public string name;
    /// <summary>
    /// El dialogo que se tendrá con ese personaje
    /// </summary>
    [TextArea(3,10)]
    public string[] sentenceList;
}
