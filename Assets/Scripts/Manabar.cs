﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Clase que maneja el maná del Player
/// </summary>
public class Manabar : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Image"/> que hace referecia a la imagen del maná
    /// </summary>
    public Image imgMana;
    /// <summary>
    /// Float que indica el maná del Player
    /// </summary>
    public float Mana { set; get; }
    /// <summary>
    /// Float que indica el maná maxima del Player
    /// </summary>
    float maxMana = 100f;
    /// <summary>
    /// Floats que gestionan el tiempo para la recuperación del maná
    /// </summary>
    float timer, waitTime = 2;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player playerScript;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        maxMana = 100f + playerScript.Magia;
        Mana = PlayerPrefs.GetFloat("mana",maxMana);
        Time.timeScale = 1;
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        maxMana = 100f + playerScript.Magia;
        timer += Time.deltaTime;

            if (timer > waitTime)
            {
                Mana += 5;

                timer = timer - waitTime;    
            }
        
        if (Mana>=maxMana) {
            Mana = maxMana;
        }
        imgMana.transform.localScale = new Vector2(Mana / maxMana, 1);
    }
    /// <summary>
    /// Función que se encarga de restar el maná con el parametro pasado 
    /// </summary>
    /// <param name="amount">La cantidad que se resta al maná</param>
    public void Gastar(float amount) {
        Mana = Mathf.Clamp(Mana-amount,0f,maxMana);
    }
    /// <summary>
    /// Se encarga de administrar si se puede o no usar maná
    /// </summary>
    /// <param name="cant">La cantidad que se usara de maná</param>
    /// <returns></returns>
    public bool Usable(float cant) {
        if (cant <= Mana)
        {
            return true;
        }
        else {
            return false;
        }
    }
}
