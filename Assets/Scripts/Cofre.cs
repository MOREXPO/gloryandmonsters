﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
/// <summary>
/// Clase que maneja el <see cref="GameObject"/> Cofre.
/// </summary>
public class Cofre : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Objeto de tipo <see cref="CircleCollider2D"/>
    /// </summary>
    CircleCollider2D circulo;
    /// <summary>
    /// Objeto de tipo <see cref="PlayableDirector"/>
    /// </summary>
    [SerializeField] private PlayableDirector secuenciaMoneda;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        anim = GetComponent<Animator>();
        circulo = GetComponent<CircleCollider2D>();
        if (PlayerPrefs.GetInt("MonedaRecogida",0)==1) {
            gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// Cuando el Player choque contra el collider del cofre se inicia la animación de que se abre el cofre.
    /// </summary>
    /// <param name="collision">Objeto que choca contra el collider del Cofre</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag=="Player") {
            PlayerPrefs.SetInt("MonedaRecogida",1);
            anim.SetTrigger("approach");
            circulo.enabled = false;
            secuenciaMoneda.Play();
        }
    }
}
