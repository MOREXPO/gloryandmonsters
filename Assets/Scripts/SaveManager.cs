﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// Clase que gestiona el guardar y cargar
/// </summary>
public class SaveManager : MonoBehaviour
{
    /// <summary>
    /// Variable estatica para referenciarnos a la propia clase
    /// </summary>
    public static SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="GameManager"/>
    /// </summary>
    private GameObject gameManager;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/> que hace referencia al Player
    /// </summary>
    private GameObject Player;
    /// <summary>
    /// Objetos de tipo <see cref="GameObject"/> que manejan la UI
    /// </summary>
    private GameObject UI,Healthbar,Manabar;
    /// <summary>
    /// El numero de enemigos muertos
    /// </summary>
    private int enemigosMuertos;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de los enemigos
    /// </summary>
    private List<string> enemigosString;
    /// <summary>
    /// Objeto de tipo <see cref="List{GameObject}"/> para guardar los GameObject de los enemigos
    /// </summary>
    private List<GameObject> enemigosObject;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de los enemigos y que no se repitan
    /// </summary>
    private List<string> contenedorEnemyString;
    /// <summary>
    /// El numero de vidas recogidas
    /// </summary>
    private int vidasRecogidas;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de las vidas
    /// </summary>
    private List<string> vidasString;
    /// <summary>
    /// Objeto de tipo <see cref="List{GameObject}"/> para guardar los GameObject de las vidas
    /// </summary>
    private List<GameObject> vidasObject;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de las vidas y que no se repitan
    /// </summary>
    private List<string> contenedorVidaString;
    /// <summary>
    /// El numero de items destruibles que han sido destruidos
    /// </summary>
    private int destroyablesDestruidos;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de los items destruibles
    /// </summary>
    private List<string> destroyablesString;
    /// <summary>
    /// Objeto de tipo <see cref="List{GameObject}"/> para guardar los GameObject de los items destruibles
    /// </summary>
    private List<GameObject> destroyablesObject;
    /// <summary>
    /// Objeto de tipo <see cref="List{string}"/> para guardar el nombre de los GameObject de los items destruibles y que no se repitan
    /// </summary>
    private List<string> contenedorDestroyableString;
    /// <summary>
    /// Gameobject del Background en el que se encuentre el Player actualmente
    /// </summary>
    private GameObject EscenarioActual { set; get; }
    /// <summary>
    /// Booleana que indica si se entro como nuevo juego o no
    /// </summary>
    public bool NuevoJuego { set; get; }
    /// <summary>
    /// La cantidad de enemigos que estan muertos
    /// </summary>
    public int ContadorEnemigos { set; get; }
    /// <summary>
    /// La cantidad de vidas que se han recogido
    /// </summary>
    public int ContadorVidas { set; get; }
    /// <summary>
    /// La cantidad de items destruibles que han sido destruidos
    /// </summary>
    public int ContadorDestroyables { set; get; }
    /// <summary>
    /// Funcion que se llama al cargar la instancia del script
    /// </summary>
    private void Awake()
    {
        if (saveManager == null)
        {
            saveManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (saveManager != this)
        {
            Destroy(gameObject);
        }

    }
    /// <summary>
    /// Se instancian las variables de la UI
    /// </summary>
    public void cargarUI() {
        UI = GameObject.Find("UI");
        Healthbar = GameObject.Find("Healthbar");
        Manabar = GameObject.Find("Manabar");
    }
    /// <summary>
    /// Se ejecuta cuando se inicia el juego y se cargan todos los componentes
    /// </summary>
    public void Cargar() {
        Player = GameObject.Find("Player");
        contenedorEnemyString = new List<string>();
        contenedorVidaString = new List<string>();
        contenedorDestroyableString = new List<string>();
        ContadorEnemigos = PlayerPrefs.GetInt("cont", 0);
        ContadorVidas = PlayerPrefs.GetInt("contV", 0);
        ContadorDestroyables = PlayerPrefs.GetInt("contD", 0);
        if (!NuevoJuego) {
            GameObject.Find("GameManager").GetComponent<GameManager>().saltarIntro();
            GameObject.Find("GameManager").GetComponent<GameManager>().Nombre = PlayerPrefs.GetString("Nombre");
            GameObject.Find("escenarioComienzo").SetActive(false);


            enemigosString = new List<string>();
            enemigosObject = GameObject.FindGameObjectsWithTag("Enemy").ToList();
            for (int i=0; i<enemigosObject.Count;i++) {
                enemigosString.Add(enemigosObject[i].name);
            }
            enemigosMuertos = PlayerPrefs.GetInt("EnemigosMuertos", 0);
            print("Enemigos muertos:"+enemigosMuertos);
            if (enemigosMuertos!=0) {
                for (int i = 1; i <= enemigosMuertos; i++)
                {
                    if (enemigosString.Contains(PlayerPrefs.GetString(i + ""))) ;
                    {
                        print(PlayerPrefs.GetString(i + ""));
                        GameObject.Find(PlayerPrefs.GetString(i + "")).SetActive(false);
                    }
                }
            }


            vidasString = new List<string>();
            vidasObject = GameObject.FindGameObjectsWithTag("Vida").ToList();
            for (int i = 0; i < vidasObject.Count; i++)
            {
                vidasString.Add(vidasObject[i].name);
            }
            vidasRecogidas = PlayerPrefs.GetInt("VidasRecogidas", 0);
            print("Vidas recogidas:" + vidasRecogidas);
            if (vidasRecogidas != 0)
            {
                for (int i = 1; i <= vidasRecogidas; i++)
                {
                    if (vidasString.Contains(PlayerPrefs.GetString(i + "V"))) ;
                    {
                        print(PlayerPrefs.GetString(i + "V"));
                        GameObject.Find(PlayerPrefs.GetString(i + "V")).SetActive(false);
                    }
                }
            }


            destroyablesString = new List<string>();
            destroyablesObject = GameObject.FindGameObjectsWithTag("Destroyable").ToList();
            for (int i = 0; i < destroyablesObject.Count; i++)
            {
               destroyablesString.Add(destroyablesObject[i].name);
            }
            destroyablesDestruidos = PlayerPrefs.GetInt("DestroyablesDestruidos", 0);
            print("Destroyables destruidas:" + destroyablesDestruidos);
            if (destroyablesDestruidos != 0)
            {
                for (int i = 1; i <= destroyablesDestruidos; i++)
                {
                    if (destroyablesString.Contains(PlayerPrefs.GetString(i + "D"))) ;
                    {
                        print(PlayerPrefs.GetString(i + "D"));
                        GameObject.Find(PlayerPrefs.GetString(i + "D")).SetActive(false);
                    }
                }
            }


            Player.transform.position = new Vector3(PlayerPrefs.GetFloat("playerX"), PlayerPrefs.GetFloat("playerY"), PlayerPrefs.GetFloat("playerZ"));
            EscenarioActual = GameObject.Find(PlayerPrefs.GetString("escenarioActual"));
            if (EscenarioActual == null)
            {
                GameObject.Find("MusicaAmbiente").GetComponent<AudioSource>().Play();
                GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 4;
            }
            else if (EscenarioActual.name == "Background_Escenario7") {
                GameObject.Find("TimelineFinal").SetActive(false);
                gameManager = GameObject.Find("GameManager");
                gameManager.GetComponent<GameManager>().ReproducirMusicaFinal();
                GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 6;
            } else if (EscenarioActual.name == "Background_Escenario1") {
                GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 4;
                GameObject.Find("MusicaAmbiente").GetComponent<AudioSource>().Play();
            }
            else
            {
                GameObject.Find("CM_Player").GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 6;
                GameObject.Find("MusicaAmbiente").GetComponent<AudioSource>().clip = GameObject.Find(PlayerPrefs.GetString("WarpActual", "warp entrada-pueblo")).GetComponent<Warp>().audioClip;
                GameObject.Find("MusicaAmbiente").GetComponent<AudioSource>().Play();
            }
            GameObject.Find("CM_Player").GetComponent<MainCamera>().setBound(EscenarioActual);

        }
    }
    /// <summary>
    /// Se guarda el progreso del usuario en esta función
    /// </summary>
    public void Guardar() {
        PlayerPrefs.SetString("Nombre", GameObject.Find("GameManager").GetComponent<GameManager>().Nombre);
        PlayerPrefs.SetFloat("playerX", Player.transform.position.x);
        PlayerPrefs.SetFloat("playerY", Player.transform.position.y);
        PlayerPrefs.SetFloat("playerZ", Player.transform.position.z);
        PlayerPrefs.SetFloat("VidaPlayer",Player.GetComponent<Player>().Vida);
        PlayerPrefs.SetInt("ResistenciaPlayer", Player.GetComponent<Player>().Resistencia);
        PlayerPrefs.SetInt("FuerzaPlayer", Player.GetComponent<Player>().Fuerza);
        PlayerPrefs.SetInt("MagiaPlayer", Player.GetComponent<Player>().Magia);

        PlayerPrefs.SetInt("NivelVidaPlayer", Player.GetComponent<Player>().NivelVida);
        PlayerPrefs.SetInt("NivelResistenciaPlayer", Player.GetComponent<Player>().NivelResistencia);
        PlayerPrefs.SetInt("NivelFuerzaPlayer", Player.GetComponent<Player>().NivelFuerza);
        PlayerPrefs.SetInt("NivelMagiaPlayer", Player.GetComponent<Player>().NivelMagia);
        if (UI!=null) {
            PlayerPrefs.SetInt("cantNivel", UI.GetComponent<Ui>().CantNivel);
            PlayerPrefs.SetFloat("exp", UI.GetComponent<Ui>().Exp);
            PlayerPrefs.SetFloat("maxExp", UI.GetComponent<Ui>().MaxExp);
            PlayerPrefs.SetFloat("hp", Healthbar.GetComponent<Healthbar>().Hp);
            PlayerPrefs.SetFloat("mana", Manabar.GetComponent<Manabar>().Mana);
        }
        PlayerPrefs.SetInt("cont", ContadorEnemigos);
        PlayerPrefs.SetInt("contD", ContadorDestroyables);
        PlayerPrefs.SetInt("contV", ContadorVidas);
    }
    /// <summary>
    /// Se borran todos los datos guardados
    /// </summary>
    public void BorrarTodo() {
        PlayerPrefs.DeleteAll();
    }
    /// <summary>
    /// Función que sirve para contar un enemigo muerto y guardar el nombre del enemigos en el PlayerPrefs
    /// </summary>
    /// <param name="nameEnemy">El nombre del GameObject del enemigo</param>
    public void EnemyMuerto(string nameEnemy) {
        if (!contenedorEnemyString.Contains(nameEnemy))
        {
            contenedorEnemyString.Add(nameEnemy);
            ContadorEnemigos++;
            PlayerPrefs.SetString(ContadorEnemigos + "", nameEnemy);
            print(ContadorEnemigos + ":" + PlayerPrefs.GetString(ContadorEnemigos + ""));
            PlayerPrefs.SetInt("EnemigosMuertos", ContadorEnemigos);
        }
        else {
            print("ENEMY REPETIDO");
        }
        
    }
    /// <summary>
    /// Función que sirve para contar una vida recogida y guardar el nombre de al vida en el PlayerPrefs
    /// </summary>
    /// <param name="nameVida">El nombre del GameObject de la vida</param>
    public void VidaRecogida(string nameVida)
    {
        if (!contenedorVidaString.Contains(nameVida))
        {
            contenedorVidaString.Add(nameVida);
            ContadorVidas++;
            PlayerPrefs.SetString(ContadorVidas + "V", nameVida);
            print(ContadorVidas + ":" + PlayerPrefs.GetString(ContadorVidas + "V"));
            PlayerPrefs.SetInt("VidasRecogidas", ContadorVidas);
        }
        else
        {
            print("VIDA REPETIDA");
        }

    }
    /// <summary>
    /// Función que sirve para contar un item destruido y guardar el nombre del item en el PlayerPrefs
    /// </summary>
    /// <param name="nameDestroy">El nombre del GameObject del item</param>
    public void DestroyableDestruido(string nameDestroy)
    {
        if (!contenedorDestroyableString.Contains(nameDestroy))
        {
            contenedorDestroyableString.Add(nameDestroy);
            ContadorDestroyables++;
            PlayerPrefs.SetString(ContadorDestroyables + "D", nameDestroy);
            print(ContadorDestroyables + ":" + PlayerPrefs.GetString(ContadorDestroyables + "D"));
            PlayerPrefs.SetInt("DestroyablesDestruidos", ContadorDestroyables);
        }
        else
        {
            print("Destroyable REPETIDO");
        }

    }
}
