﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que gestiona el Item de las vidas
/// </summary>
public class ItemVida : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/>
    /// </summary>
    public GameObject healthbar;
    /// <summary>
    /// La cantidad de vida que cura
    /// </summary>
    public float cantCura;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        audioControl = GameObject.Find("SonidoItem").GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    private void Update()
    {
        audioControl.volume = estadoJuego.Efectos;
    }
    /// <summary>
    /// Funcion que gestiona las colisiones con el ItemVida y si es el Player se cura.
    /// </summary>
    /// <param name="col">El objeto que colisiona con el ItemVida</param>
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.transform.tag == "Player")
        {
           audioControl.Play();
           saveManager.VidaRecogida(gameObject.name);
           Destroy(gameObject);
           healthbar.SendMessage("Curar", cantCura);
        }
    }
}
