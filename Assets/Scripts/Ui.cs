﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
/// <summary>
/// Clase que controla la UI, asi como la experiencia y la subida de nivel
/// </summary>
public class Ui : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="Image"/> que hace referecia a la imagen de la experiencia
    /// </summary>
    public Image imgExp;
    /// <summary>
    /// Float que indica la experiencia del Player
    /// </summary>
    public float Exp { set; get; }
    /// <summary>
    /// Float que indica la experiencia maxima del Player
    /// </summary>
    public float MaxExp { set; get; }
    /// <summary>
    /// Float que indica la experiencia que sobra despues de subir de nivel
    /// </summary>
    float expResto = 0;
    /// <summary>
    /// Int que indica el nivel del Player
    /// </summary>
    public int CantNivel { set; get; }
    /// <summary>
    /// Objeto de tipo <see cref="TextMeshProUGUI"/>
    /// </summary>
    TextMeshProUGUI nivel;
    /// <summary>
    /// Objeto de tipo <see cref="Player"/>
    /// </summary>
    public Player player;
    /// <summary>
    /// Objeto de tipo <see cref="Healthbar"/>
    /// </summary>
    public Healthbar healthbar;
    /// <summary>
    /// Los <see cref="GameObject"/> de los stats del Player
    /// </summary>
    GameObject statVida, statResistencia, statFuerza, statMagia;
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        saveManager.cargarUI();
        Exp = PlayerPrefs.GetFloat("exp", 0);
        MaxExp = PlayerPrefs.GetFloat("maxExp", 100f);
        nivel = GameObject.Find("Nivel").transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        CantNivel = PlayerPrefs.GetInt("cantNivel", 1);
        nivel.SetText("{0}", CantNivel);
        statVida = GameObject.Find("StatVida");
        statResistencia = GameObject.Find("StatResistencia");
        statFuerza = GameObject.Find("StatFuerza");
        statMagia = GameObject.Find("StatMagia");
        statVida.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelVida);
        statResistencia.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelResistencia);
        statFuerza.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}",player.NivelFuerza);
        statMagia.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelMagia);
        statVida.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Vida);
        statResistencia.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Resistencia);
        statFuerza.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Fuerza);
        statMagia.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Magia);


        statVida.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
        statResistencia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
        statFuerza.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
        statMagia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if (Exp >= MaxExp)
        {
            if (SubirStat())
            {
                expResto = Exp - MaxExp;
                Exp = expResto;
                CantNivel++;
                nivel.SetText("{0}", CantNivel);
                statVida.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Vida);
                statResistencia.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Resistencia);
                statFuerza.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Fuerza);
                statMagia.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText("{0}", player.Magia);
                MaxExp += 50;
            }
            else {
                statVida.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = true;
                statResistencia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = true;
                statFuerza.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = true;
                statMagia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = true;
                imgExp.color = Color.red;
                imgExp.transform.localScale = new Vector2(MaxExp / MaxExp, 1);
            }

        }
        else {
            statVida.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
            statResistencia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
            statFuerza.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
            statMagia.transform.GetChild(3).GetComponent<TextMeshProUGUI>().enabled = false;
            imgExp.color = Color.yellow;
            imgExp.transform.localScale = new Vector2(Exp / MaxExp, 1);
        }
        
    }
    /// <summary>
    /// Aumenta la experiencia en una determinada cantidad
    /// </summary>
    /// <param name="cantExp">La cantidad de experiencia que aumenta</param>
    public void Aumentar(float cantExp) {
            Exp = Exp + cantExp;
    }
    /// <summary>
    /// Gestiona la pulsacion de las teclas 1,2,3,4 para sibir de nivel
    /// </summary>
    /// <returns>Devuelve true si le da a alguna tecla y false si no le da</returns>
    public bool SubirStat() {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                player.Vida += 15;
                healthbar.CurarTodo();
                player.NivelVida++;
                statVida.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelVida);
                return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                player.Resistencia += 2;
                player.NivelResistencia++;
                statResistencia.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelResistencia);
            return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                player.Fuerza += 2;
                player.NivelFuerza++;
                statFuerza.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelFuerza);
            return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                player.Magia += 10;
                player.NivelMagia++;
                statMagia.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("{0}", player.NivelMagia);
            return true;
            }
        return false;
        }
}
