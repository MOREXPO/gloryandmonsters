﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// Maneja los componentes del <see cref="GameObject"/> CinematicaFinal.
/// </summary>
public class CinematicaFinal : MonoBehaviour
{
    /// <summary>
    /// Recuperamos el Timeline de la secuencia final.
    /// </summary>
    [SerializeField] private PlayableDirector secuenciaFinal;
    /// <summary>
    /// Recuperamos el objeto <see cref="Player"/>.
    /// </summary>
    private Player player;
    /// <summary>
    /// Recuperamos el objeto <see cref="EstadoJuego"/>.
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Recuperamos el objeto <see cref="AudioSource"/>.
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        audioControl = GetComponent<AudioSource>();
        
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma.
    /// </summary>
    private void Update()
    {
        audioControl.volume = estadoJuego.Efectos;
    }
    /// <summary>
    /// Inicia la secuencia final si algo choca contra el.
    /// </summary>
    /// <param name="collision">El objeto que choco contra el</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        player.enabled = false;
        secuenciaFinal.Play();
    }
}
