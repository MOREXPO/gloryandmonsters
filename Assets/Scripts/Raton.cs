﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Clase que gestiona los movimientos del raton
/// </summary>
public class Raton : MonoBehaviour
{
    /// <summary>
    /// Indica si el raton se puede mover o no
    /// </summary>
    public bool inMovement;
    /// <summary>
    /// Indica la velocidad a la que se mueve
    /// </summary>
    public float moveSpeed;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    private Rigidbody2D myRigidBody;
    /// <summary>
    /// Indica si esta en movimiento
    /// </summary>
    public bool isWalking;
    /// <summary>
    /// El tiempo que caminan
    /// </summary>
    public float walkTime;
    /// <summary>
    /// Variable auxiliar de <see cref="walkTime"/>
    /// </summary>
    private float walkCounter;
    /// <summary>
    /// El tiempo que espera antes de caminar
    /// </summary>
    public float waitTime;
    /// <summary>
    /// Variable auxiliar de <see cref="waitTime"/>
    /// </summary>
    private float waitCounter;
    /// <summary>
    /// Controla la dirección en la que camina
    /// </summary>
    private int WalkDirection;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        waitCounter = waitTime;
        walkCounter = walkTime;
        if (inMovement) ChooseDirection();
        anim = GetComponent<Animator>();
    }

    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        if (!inMovement) isWalking = false;
        if (isWalking)
        {
            walkCounter -= Time.deltaTime;

            switch (WalkDirection)
            {
                case 0:
                    myRigidBody.velocity = new Vector2(0, moveSpeed);
                    break;
                case 1:
                    myRigidBody.velocity = new Vector2(moveSpeed, 0);
                    break;
                case 2:
                    myRigidBody.velocity = new Vector2(0, -moveSpeed);
                    break;
                case 3:
                    myRigidBody.velocity = new Vector2(-moveSpeed, 0);
                    break;
            }
            anim.speed = 1;
            anim.SetFloat("movX", myRigidBody.velocity.x);
            anim.SetFloat("movY", myRigidBody.velocity.y);
            anim.SetBool("walking", true);
            if (walkCounter < 0)
            {
                isWalking = false;
                waitCounter = waitTime;
            }
        }
        else
        {
            waitCounter -= Time.deltaTime;
            myRigidBody.velocity = Vector2.zero;
            if (waitCounter < 0)
            {
                ChooseDirection();
            }
        }
    }
    /// <summary>
    /// Función que realiza el cambio de dirección y empieza a caminar
    /// </summary>
    public void ChooseDirection()
    {
        WalkDirection = Random.Range(0, 4);
        isWalking = true;
        walkCounter = walkTime;
    }

}
