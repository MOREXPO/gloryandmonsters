﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
/// <summary>
/// Clase que gestiona la moneda
/// </summary>
public class Moneda : MonoBehaviour
{
    /// <summary>
    /// La animación del laberinto
    /// </summary>
    [SerializeField] private PlayableDirector secuenciaMapaLaberinto;
    /// <summary>
    /// Objeto de tipo <see cref="Ui"/>
    /// </summary>
    public Ui ui;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioControl;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    private void Start()
    {
        audioControl = GameObject.Find("SonidoItem").GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    private void Update()
    {
        audioControl.volume = estadoJuego.Efectos;
    }
    /// <summary>
    /// Funcion que gestiona las colisiones con la moneda y le otorga experiencia al Player.
    /// </summary>
    /// <param name="col">El objeto que colisiona con la moneda</param>
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.transform.tag == "Player")
        {
            secuenciaMapaLaberinto.Play();
            ui.Aumentar(2000);
            audioControl.Play();
            Destroy(gameObject);
        }
    }
}
