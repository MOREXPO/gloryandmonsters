﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
/// <summary>
/// Clase que gestiona el Player
/// </summary>
public class Player : MonoBehaviour
{
    /// <summary>
    /// La velocidad de movimiento del Player
    /// </summary>
    public float speed = 4f;
    /// <summary>
    /// Objeto de tipo <see cref="Animator"/>
    /// </summary>
    Animator anim;
    /// <summary>
    /// Recuperamos el componente de cuerpo rígido
    /// </summary>
    Rigidbody2D rb2d;
    /// <summary>
    /// La posicion del Player
    /// </summary>
    Vector2 mov;
    /// <summary>
    /// El collider del ataque del Player
    /// </summary>
    CircleCollider2D attackCollider;
    /// <summary>
    /// El prefab del Slash del ataque mágico
    /// </summary>
    public GameObject slashPrefab;
    /// <summary>
    /// Objeto de tipo <see cref="Manabar"/>
    /// </summary>
    public Manabar manabar;
    /// <summary>
    /// El audio del ataque con espada
    /// </summary>
    public AudioClip audioEspada;
    /// <summary>
    /// El audio del ataque con hechizo
    /// </summary>
    public AudioClip audioHechizo;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="AudioSource"/>
    /// </summary>
    private AudioSource audioPlayer;
    /// <summary>
    /// indica si te puedes mover a causa del ataque magico
    /// </summary>
    bool movePrevent;
    /// <summary>
    /// Objeto de tipo <see cref="Aura"/>
    /// </summary>
    Aura aura;
    /// <summary>
    /// El nivel de la vida del Player
    /// </summary>
    public int NivelVida { set; get; }
    /// <summary>
    /// El nivel de la resistencia del Player
    /// </summary>
    public int NivelResistencia { set; get; }
    /// <summary>
    /// El nivel de la fuerza del Player
    /// </summary>
    public int NivelFuerza { set; get; }
    /// <summary>
    /// El nivel de la magia del Player
    /// </summary>
    public int NivelMagia { set; get; }
    /// <summary>
    /// La cantidad de vida del Player
    /// </summary>
    public float Vida{ set; get; }
    /// <summary>
    /// La cantidad de resistencia del Player
    /// </summary>
    public int Resistencia{ set; get; }
    /// <summary>
    /// La cantidad de fuerza del Player
    /// </summary>
    public int Fuerza{ set; get; }
    /// <summary>
    /// La cantidad de magia del Player
    /// </summary>
    public int Magia{ set; get; }
    /// <summary>
    /// Funcion que se llama al cargar la instancia del script
    /// </summary>
    private void Awake()
    {
        Assert.IsNotNull(slashPrefab);
        Vida = PlayerPrefs.GetFloat("VidaPlayer",100f);
        Resistencia = PlayerPrefs.GetInt("ResistenciaPlayer", 0);
        Fuerza = PlayerPrefs.GetInt("FuerzaPlayer", 1);
        Magia = PlayerPrefs.GetInt("MagiaPlayer", 3);
        NivelVida = PlayerPrefs.GetInt("NivelVidaPlayer", 1);
        NivelResistencia = PlayerPrefs.GetInt("NivelResistenciaPlayer", 1);
        NivelFuerza = PlayerPrefs.GetInt("NivelFuerzaPlayer", 1);
        NivelMagia = PlayerPrefs.GetInt("NivelMagiaPlayer", 1);
    }
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        attackCollider = transform.GetChild(0).GetComponent<CircleCollider2D>();
        attackCollider.enabled = false;
        aura = transform.GetChild(1).GetComponent<Aura>();
        audioPlayer = GetComponent<AudioSource>();
        estadoJuego = GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        
    }
    /// <summary>
    /// Funcion que se llama en cada fotograma
    /// </summary>
    void Update()
    {
        audioPlayer.volume = estadoJuego.Efectos;
        Movimiento();
        Animaciones();
        AtaqueEspada();
        SlashAttack();
        PreventMovement();
    }
    /// <summary>
    /// Movemos al personaje hacia una ubicación
    /// </summary>
    private void FixedUpdate()
    {
        rb2d.MovePosition(rb2d.position+mov*speed*Time.deltaTime);
    }
    /// <summary>
    /// Funcion que gestiona el movimiento
    /// </summary>
    void Movimiento() {
        mov = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }
    /// <summary>
    /// Funcion que gestiona las animaciones
    /// </summary>
    void Animaciones() {
        if (mov != Vector2.zero)
        {
            anim.SetFloat("movX", mov.x);
            anim.SetFloat("movY", mov.y);
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }
    }
    /// <summary>
    /// Funcion que gestiona el ataque con espada
    /// </summary>
    void AtaqueEspada() {
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        bool attacking = stateInfo.IsName("Player_Attack");
        if (Input.GetKeyDown("space") && !attacking)
        {
            anim.SetTrigger("attacking");
            audioPlayer.clip = audioEspada;
            audioPlayer.Play();
        }

        if (mov != Vector2.zero) attackCollider.offset = new Vector2(mov.x / 2, mov.y / 2);

        if (attacking)
        {
            float playbackTime = stateInfo.normalizedTime;
            if (playbackTime > 0.33 && playbackTime < 0.66) attackCollider.enabled = true;
            else attackCollider.enabled = false;
        }
    }
    /// <summary>
    /// Funcion que gestiona que no nos podamos mover durante el ataque mágico
    /// </summary>
    void PreventMovement() {
        if (movePrevent) {
            mov = Vector2.zero;
        }
    }
    /// <summary>
    /// Funcion que gestiona el ataque mágico
    /// </summary>
    void SlashAttack() {
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        bool loading = stateInfo.IsName("Player_Slash");
        if (Input.GetKeyDown(KeyCode.LeftAlt) && manabar.Usable(20)) {
            anim.SetTrigger("loading");
            aura.AuraStart();
        }
        else if(Input.GetKeyUp(KeyCode.LeftAlt) && manabar.Usable(20))
        {
            anim.SetTrigger("attacking");
            if (aura.IsLoaded())
            {
                audioPlayer.clip = audioHechizo;
                audioPlayer.Play();
                float angle = Mathf.Atan2(anim.GetFloat("movY"), anim.GetFloat("movX")) * Mathf.Rad2Deg;
                GameObject slashObj = Instantiate(slashPrefab, transform.position, Quaternion.AngleAxis(angle, Vector3.forward));
                Slash slash = slashObj.GetComponent<Slash>();
                slash.mov.x = anim.GetFloat("movX");
                slash.mov.y = anim.GetFloat("movY");
                manabar.Gastar((100+Magia)/4);
            }
            aura.AuraStop();
            StartCoroutine(EnableMovementAfter(0.4f));
        }

        if (loading) {
            movePrevent = true;
        }
       
    }
    /// <summary>
    /// Función que gestiona que cuando acabemos el ataque magico tengamos una breve pausa en el movimiento
    /// </summary>
    /// <param name="seconds">los segundos que tarda en moverse</param>
    /// <returns>El retardo de tiempo para poder moverse</returns>
    IEnumerator EnableMovementAfter(float seconds) {
        yield return new WaitForSeconds(seconds);
        movePrevent = false;
    }
}
