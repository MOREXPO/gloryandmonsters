﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Clase que controla el Menu Principal
/// </summary>
public class MenuPrincipal : MonoBehaviour
{
    /// <summary>
    /// Objeto de tipo <see cref="SaveManager"/>
    /// </summary>
    private SaveManager saveManager;
    /// <summary>
    /// Objeto de tipo <see cref="EstadoJuego"/>
    /// </summary>
    private EstadoJuego estadoJuego;
    /// <summary>
    /// Objeto de tipo <see cref="GameObject"/>
    /// </summary>
    public GameObject cargar;
    /// <summary>
    /// Funcion que se llama al inicio de la ejecución del script.
    /// </summary>
    void Start()
    {
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        estadoJuego=GameObject.Find("EstadoJuego").GetComponent<EstadoJuego>();
        if (PlayerPrefs.GetInt("NuevoJuego",1)==1) {
            cargar.SetActive(false);
        }
    }
    /// <summary>
    /// Función que borra todo el guardado y accede como nuevo juego
    /// </summary>
    public void EmpezarJuego(){
        saveManager.BorrarTodo();
        saveManager.NuevoJuego = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    /// <summary>
    /// Función que accede al juego desde el ultimo punto en el que se salio
    /// </summary>
    public void Cargar()
    {
        saveManager.NuevoJuego = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    /// <summary>
    /// Cierra el juego guardando los ajustes de musica y efectos
    /// </summary>
    public void Cerrarjuego(){
        estadoJuego.GuardarMusicaEfectos();
        Application.Quit();
    }
}
